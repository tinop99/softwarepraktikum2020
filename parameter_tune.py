import numpy as np
import random
import pandas as pd

def genetic_algorithm(n,t,cross_prob,m,daten) :
	########################################## Read Data

	data = pd.read_csv(daten, delimiter=';') # the dataset can also be changed manually
	z = list(data.columns)
	########################################## Initialize Population
	population = np.zeros((n,11)) # n = population size
	for i in range(n):           
		dc_size = random.randint(1,10)       # length of distributed classifier (individual)
		population[i][10] = dc_size	
		tmp = random.sample(range(2, len(z)), dc_size)     # 0. & 1. column are ID and Annotation 
		for j in range(dc_size):                           # input can be negated (logical "NOT")
			tmp2 = random.choice([-1,1])
			population[i][j] = tmp[j]*tmp2

	########################################## best individual as intermediate result
	best_individual = [0]*11
	################################################################################################################################################## Iteration / Terminiation
	for q in range(100):
	
	########################################## Evaluation
		score = [0]*n # scores for each individual
		for i in range(n):   
			
			result = np.zeros(int(population[i][10]))    
			tru = 0  # counter for correct classification (TP&TN)
			for k in range(len(data)): 				
				for j in range(int(population[i][10])):	
					
					if population[i][j] > 0 :
						
						result[j] = data[z[int(population[i][j])]][k]
						
					elif population[i][j] < 0 :
						
						result[j] = (data[z[-1*int(population[i][j])]][k] - 1) * -1  #1->0 and 0->1
						
					else : 	
						break
				
				if np.count_nonzero(result) > 0.5*population[i][10] : # majority vote
					decision = 1
				else : 
					decision = 0
					
				if decision == int(data['Annots'][k]) :
					tru += 1

			score[i] = tru/len(data)   #accuracy of individual i

		best_individual[10] = max(score)
		tmp_best = score.index(max(score))
		for i in range (10):
			best_individual[i] = population[tmp_best][i]
			
		###################################### Selektion (Tunier)
		new_population = np.zeros((n,11)) # child population

		for i in range(n):
				
			teilnehmer = random.sample(range(n),int(n*t))  # randomly choose participants
			winner = [-1,-1]      # initialize winner -> [score,index of individual] 
			for j in range(int(n*t)):		
				if score[teilnehmer[j]] > winner[0] : 
					winner[0] = score[teilnehmer[j]]
					winner[1] = teilnehmer[j]
					
			new_population[i] = population[teilnehmer[j]]

		###################################### Crossover
		foo = random.sample(range(n),n) # random arragement of all individuals/parents

		for i in range(int(n/2)):
			
			prob = random.random() # random floating number between 0 and 1 
			if prob < cross_prob :
				parent1 = foo[i*2]
				parent2 = foo[i*2+1]
				child1 = np.zeros(11)
				child2 = np.zeros(11)	
				if new_population[parent1][10] == new_population[parent2][10] : # same sized parents
				##	print("same size")
					child_size = new_population[parent1][10]
					child1[10] = child_size
					child2[10] = child_size
					loo = np.full(int(child_size*2),parent1) # big array of size child_size*2; values = indeces of parent1 and parent2
					loo[:loo.size//2:1]=parent2
					random.shuffle(loo) # shuffle array and only consider the first half of values
					for j in range(int(child_size)) :
						child1[j] = new_population[loo[j]][j] # consists of the values from the respective parent of the first half of values
						child2[j] = new_population[parent1+parent2-loo[j]][j] # child_2 consists of the respective counterparts
					# check if the children have all different values
					tmp_child = child1[0:int(child1[10])]
					tmp_child = list(set(tmp_child)) # list of all unique elements of child1
					if len(tmp_child) != int(child1[10]):  
						child1 = [0,0,0,0,0,0,0,0,0,0,0]
						child1[10] = len(tmp_child)
						random.shuffle(tmp_child)
						for l in range(len(tmp_child)):
							child1[l] = tmp_child[l]
					tmp_child = child2[0:int(child2[10])]
					tmp_child = list(set(tmp_child))
					if len(tmp_child) != int(child2[10]):  
						child2 = [0,0,0,0,0,0,0,0,0,0,0]
						child2[10] = len(tmp_child)
						random.shuffle(tmp_child)
						for l in range(len(tmp_child)):
							child2[l] = tmp_child[l]
		
					new_population[parent1] = child1
					new_population[parent2] = child2
				
				else : # new_population[parent1] > new_population[parent2] 
					##print("diff size")
					if new_population[parent2][10] > new_population[parent1][10]: # if !(new_population[parent1] > new_population[parent2]) swap parent1 and parent2
						tmp_parent = parent2
						parent2 = parent1
						parent1 = tmp_parent
				
					child_size = new_population[parent2][10] # smaller dc_size
					child2[10] = child_size
					child1[10] = new_population[parent1][10]
					loo = np.full(int(child_size*2),parent1)
					loo[:loo.size//2:1]=parent2
					random.shuffle(loo)
					for j in range(int(child_size)):
						child1[j] = new_population[loo[j]][j]
						child2[j] = new_population[parent1+parent2-loo[j]][j]
					for k in range(int(child_size),int(child1[10])): # append the rest
						child1[k] = new_population[parent1][k]
					# check if the children have all different values
					tmp_child = child1[0:int(child1[10])]
					tmp_child = list(set(tmp_child))
					if len(tmp_child) != int(child1[10]):  
						child1 = [0,0,0,0,0,0,0,0,0,0,0]
						child1[10] = len(tmp_child)
						random.shuffle(tmp_child)
						for l in range(len(tmp_child)):
							child1[l] = tmp_child[l]
					tmp_child = child2[0:int(child2[10])]
					tmp_child = list(set(tmp_child))
					if len(tmp_child) != int(child2[10]):  
						child2 = [0,0,0,0,0,0,0,0,0,0,0]
						child2[10] = len(tmp_child)
						random.shuffle(tmp_child)
						for l in range(len(tmp_child)):
							child2[l] = tmp_child[l]		

					new_population[parent1] = child1
					new_population[parent2] = child2

		###################################### Mutation

		for i in range (n):
			prob = random.random()
			if prob < m :
				mutation = random.choice(['insert', 'delete', 'swap'])

				if mutation == 'insert' :
					if new_population[i][10] < 10:
						mutation_site = int(new_population[i][10])
					else : 
						mutation_site = random.randint(0,9)
					A = list(range(2,len(z)))
					B = new_population[i][0:int(new_population[i][10])] 
					tmp_mutation = random.choice([-1,1]) # new sc negated or non-negated
					new_population[i][mutation_site] = tmp_mutation * random.choice([x for x, x in enumerate(A) if x not in B[0:int(new_population[i][10])]]) # new value cannot an already existing one(B)
					if new_population[i][10] < 10 :
						new_population[i][10] += 1
						
				elif mutation == 'delete' :
					if int(new_population[i][10]) > 0:
						mutation_site = random.randint(0,int(new_population[i][10])-1)
						tmp_mutation = new_population[i][int(new_population[i][10])-1]
						new_population[i][mutation_site] = tmp_mutation
						new_population[i][int(new_population[i][10])-1] = 0
						new_population[i][10] -= 1

				elif mutation == 'swap' :
					if int(new_population[i][10]) > 0:
						mutation_site = random.randint(0,int(new_population[i][10]-1))
						A = list(range(2,len(z)))
						B = new_population[i][0:int(new_population[i][10])]
						tmp_mutation = random.choice([-1,1])
						new_population[i][mutation_site] = tmp_mutation * random.choice([x for x, x in enumerate(A) if x not in B[0:int(new_population[i][10])]])
					
				else :
					print("mutation error")
 
		population = new_population # overwrite the population for the next iteration

	##print(new_population)
	print("bestes Inividuum: ", best_individual) # best individual
	return best_individual[10] # score of best individual

daten = input("data_filename: ")
parameter_sets = np.zeros((100,4))
avr_score_sets = [0]*100
avr_score = [0]*5

for p in range(100):
	
	parameter_sets[p][0] = int(random.randrange(50,300,50))	# population size n
	parameter_sets[p][1] = random.randrange(1,5,1) /10 # tournament size t
	parameter_sets[p][2] = random.randrange(1,10,1) /10 # crossover probability cross_prob
	parameter_sets[p][3] = random.randrange(1,10,1) /10 # mutation probability m

print (parameter_sets)

for q in range (100) : 
	
	for u in range (5):
		
		avr_score[u] = genetic_algorithm ( int(parameter_sets[q][0]),parameter_sets[q][1],parameter_sets[q][2],parameter_sets[q][3],daten)
	
	avr_score_sets[q] = sum(avr_score) / 5 # 5 = len(avr_score)
	print("parameterset ", q, " : ", avr_score)
print(max(avr_score_sets),avr_score_sets.index(max(avr_score_sets))) # best parameter set