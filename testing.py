import numpy as np
import pandas as pd

def testing(best_classifier, daten):

	data = pd.read_csv(daten, delimiter=';') # or change manually
	z = list(data.columns)
	
	result = np.zeros(int(best_classifier[10]))    
	tru = 0  # counter for correct classification (TP&TN)
	for k in range(len(data)): 			
		for j in range(int(best_classifier[10])):	
			
			if best_classifier[j] > 0 :
					
				result[j] = data[z[int(best_classifier[j])]][k]
					
			elif best_classifier[j] < 0 :
					
				result[j] = (data[z[-1*int(best_classifier[j])]][k] - 1) * -1  #1->0 and 0->1
					
			else : 	
				break
		
		if np.count_nonzero(result) > 0.5*best_classifier[10] : # majority vote
			decision = 1
		else : 
			decision = 0
				
		if decision == int(data['Annots'][k]) :
			tru += 1

	score = tru/len(data)   #accuracy of individual i
			
	print(score)
	return score
	
best_individual = list(map(int, input("best individual: ").split(','))) # Input: 51,107,69,-68,37,17,-88,-106,0,0,8
daten = input("data_filename: ") #Input: liver_test_1.csv
#best_individual = [51, 107, 69, -68, 37, 17, -88, -106, 0, 0, 8] # changed manually
testing(best_individual, daten)
